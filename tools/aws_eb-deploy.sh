#!/usr/bin/env bash
# Helper script to create an artifact zip file and deploy the new version to 
# aws beanstalk. Using this because eb deploy is not handling git submodules, 
# even though docs say it should.

pipenv lock --requirements > requirements.txt

git submodule update --remote
rm -vf myapp.zip
zip -FSr myapp.zip . \
    -x '.cfg' \
    -x '.git*' \
    -x '*.swp' \
    -x '*.swo' \
    -x '.curlrc*' \
    -x 'python_restapi*' \
    -x 'dist/*' \
    -x 'build/*' \
    -x '__pycache__/*' \
    -x 'restapilib/__pycache__/*' \
    -x '.elasticbeanstalk/app_versions/*' \
    -x '.bak/*' \
    -x 'Pipfile*' \
    -x 'db.sqlite'

myEnv=${RESTAPI_ENV:-staging}

zip -u myapp.zip "$myEnv.cfg"

cp -v ".elasticbeanstalk/${myEnv}_config.yml" .elasticbeanstalk/config.yml

cp -v .ebextensions/myapp.config .ebextensions/defaults_myapp.config
cp -v ".ebextensions/${myEnv}_myapp.config" .ebextensions/myapp.config

pipenv run eb deploy "$@"

cp -v .ebextensions/defaults_myapp.config .ebextensions/myapp.config
