from os import getcwd, environ
from urllib.parse import quote_plus
from datetime import timedelta
from functools import update_wrapper

from flask import current_app, make_response, request, jsonify

from restapilib import APP_NAME
from restapilib.models import db, User, Organisation


def get_database_uri(config):
    # First check if we're using RDS and ignore all other settings
    if environ.get('RDS_DB_NAME', None):
        db_uri = '{method}://{username}:{password}@{hostname}:{port}/{name}'.format(
            method=config.get(APP_NAME, 'db_method'),
            username=environ.get('RDS_USERNAME'),
            password=quote_plus(environ.get('RDS_PASSWORD')),
            hostname=environ.get('RDS_HOSTNAME'),
            port=environ.get('RDS_PORT'),
            name=environ.get('RDS_DB_NAME')
        )
        return db_uri

    # Now sqlite
    cur_cwd = getcwd()+'/'

    db_uri = '{db_method}:///{db_path}{db_conn_str}'.format(
        db_method=config.get(APP_NAME, 'db_method'),
        db_conn_str=config.get(APP_NAME, 'db_conn_str'),
        db_path=cur_cwd
    )

    # Lastly postgres and mysql
    sql_methods = ['postgresql', 'mysql+pymysql']
    if config.get(APP_NAME, 'db_method') in  sql_methods:
        db_uri = '{method}://{username}:{password}@{hostname}:{port}/{database}'.format(
            method=config.get(APP_NAME, 'db_method'),
            username=config.get(APP_NAME, 'db_username'),
            password=quote_plus(config.get(APP_NAME, 'db_password')),
            hostname=config.get(APP_NAME, 'db_hostname'),
            database=config.get(APP_NAME, 'db_name'),
            port=config.get(APP_NAME, 'db_port')
        )
        if config.get(APP_NAME, 'db_method') == 'postgresql':
            if not config.getboolean(APP_NAME, 'db_sslmode'):
                db_uri += '?sslmode=disable'
    return db_uri


def create_admin_organisation(db):
    organisation = Organisation.query.order_by(
        Organisation.created.desc()
    ).first()
    if organisation:
        return organisation
    organisation = Organisation(name='Admin Org')
    organisation.is_enabled = True
    db.session.add(organisation)
    db.session.commit()
    return organisation


def create_admin_user(db, **kw):
    org_id = kw.get('org_id')
    username = kw.get('username')
    password = kw.get('password')

    user = User.query.filter_by(organisation=org_id).first()
    if user:
        return user
    user = User(username=username)
    user.is_enabled = True
    user.is_administrator = True
    user.organisation = org_id
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    user.password = password
    return user


def json_abort(status_code, message=''):
    response = jsonify({
        'status_code': status_code,
        'message': message
    })
    response.status_code = status_code
    return response
