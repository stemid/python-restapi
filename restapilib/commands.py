import click
from flask.cli import AppGroup
from restapilib.models import db, User, Organisation


organisation_cli = AppGroup('organisation')

def _createadminorg(name, username, password, admin):
    user = User.query.filter_by(
        username=username
    ).first()

    if user:
        raise ValueError('Username:{username} already exists'.format(
            username=username
        ))

    organisation = Organisation(
        name=name,
        is_enabled=True
    )

    db.session.add(organisation)
    db.session.commit()

    if admin:
        is_administrator = True
    else:
        is_administrator = False

    user = User(
        username=username,
        organisation=organisation.organisation_id,
        is_enabled=True,
        is_administrator=is_administrator,
        is_local_administrator=True
    )
    user.hash_password(password)

    db.session.add(user)
    db.session.commit()


@organisation_cli.command('create')
@click.option('--name', default='Admin organisation')
@click.option('--username', default='admin@localhost.tld')
@click.option('--password', default='Standard password 2018.')
@click.option('--admin', default=False, is_flag=True)
def createadminorg(name, username, password, admin):
    try:
        _createadminorg(name, username, password, admin)
    except ValueError:
        print('User already exists')
    except Exception:
        raise

