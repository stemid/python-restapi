
import os
from configparser import RawConfigParser
from argparse import ArgumentParser, FileType

from .app import create_app


argparser = ArgumentParser()
argparser.add_argument(
    '-c', '--config',
    type=FileType('r'),
    default=open(
        os.environ.get('RESTAPI_CONFIG_FILE', 'defaults.cfg')
    ),
    dest='config',
    help='Configuration file'
)

argparser.add_argument(
    '--host',
    default='localhost',
    help='Listen host'
)

argparser.add_argument(
    '--port',
    default=5000,
    help='Listen port'
)


# For restapid executable
def run_api():
    arguments = argparser.parse_args()
    application = create_app(configfile=arguments.config)
    application.run(
        host=arguments.host,
        port=arguments.port
    )
