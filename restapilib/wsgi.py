# This is mostly for use by Elastic Beanstalk or uwsgi. Requires you to set
# RESTAPI_CONFIG_FILE to some filename in your environment or it will use
# default.cfg.

from restapilib.app import create_app

application = create_app()
