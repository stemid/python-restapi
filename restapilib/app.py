import os
from configparser import RawConfigParser
from datetime import datetime

import jwt
from flask import Flask
from flask.logging import default_handler
from flask_login import LoginManager
from flask_cors import CORS
from flask_migrate import Migrate

from restapilib import APP_NAME
from restapilib.logger import AppLogger
from restapilib.models import db, User, Organisation
from restapilib.helpers import get_database_uri, json_abort
from restapilib.auth.views import restapi_auth
from restapilib.plugins import PluginManager
from restapilib.commands import organisation_cli


login_manager = LoginManager()

def create_app(**kw):
    """
    Initialization of main Flask app including all its plugins.
    """

    config = RawConfigParser()
    config.readfp(
        kw.get(
            'configfile',
            open(os.environ.get('RESTAPI_CONFIG_FILE', 'defaults.cfg'))
        )
    )

    app = Flask(APP_NAME)
    if kw.get('debug') is None:
        app.debug = config.getboolean(APP_NAME, 'debug')
    else:
        app.debug = kw.get('debug', False)

    cors_origins = config.get(APP_NAME, 'cors_origin')
    if cors_origins != '*':
        cors_origins = cors_origins.split(',')

    cors = CORS(app, resources={
        r'/*': {
            'origins': cors_origins
        }
    })

    # Init Click command
    app.cli.add_command(organisation_cli)

    applogger = AppLogger(APP_NAME, config)
    app.logger.removeHandler(default_handler)
    app.logger.addHandler(applogger.handler)
    app.logger.setLevel(applogger.log_level)
    log = app.logger

    database_uri = get_database_uri(config)
    app.config['site_config'] = config
    app.config['SQLALCHEMY_DATABASE_URI'] = database_uri
    app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = config.getboolean(
        APP_NAME, 'db_commit_on_teardown'
    )
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = config.getboolean(
        APP_NAME, 'debug'
    )
    app.config['SQLALCHEMY_ECHO'] = config.getboolean(
        APP_NAME, 'debug'
    )
    app.secret_key = config.get(APP_NAME, 'secret_key')

    log.info('Connecting to db:{database_uri}'.format(
        database_uri=database_uri
    ))
    db.init_app(app)
    db.create_all(app=app)

    login_manager.init_app(app)

    app.register_blueprint(restapi_auth, url_prefix='/auth')

    # Init all the plugins
    pm = PluginManager(config=config)
    for name, data in pm.plugins.items():
        plugin = data.get('plugin_ns')
        plugin_url_prefix = '/{plugin_name}'.format(
            plugin_name=plugin.PLUGIN_NAME
        )
        log.info('Loading plugin:{name}'.format(name=name))
        try:
            app.register_blueprint(
                plugin.app_blueprint,
                url_prefix=plugin_url_prefix
            )

            if hasattr(plugin, 'db'):
                plugin_db = getattr(plugin, 'db')
                plugin_db.init_app(app)
                plugin_db.create_all(app=app)
        except Exception as e:
            if app.debug:
                raise
            log.error('Failed to load plugin:{name}: {error}'.format(
                name=plugin.PLUGIN_NAME,
                error=str(e)
            ))
            continue

    # And finally initialize Flask-Migrate
    migrate = Migrate(app, db)

    @login_manager.user_loader
    def user_loader(user_id):
        return User.query.filter_by(user_id=user_id).first()

    @login_manager.request_loader
    def request_loader(request):
        time_now = datetime.utcnow()
        token = request.headers.get('Authorization')
        if not token:
            return None

        try:
            token_data = jwt.decode(
                token,
                config.get(APP_NAME, 'secret_key'),
                algorithm='HS256'
            )
        except jwt.exceptions.InvalidSignatureError as e:
            return None
        except Exception as e:
            return None

        user = User.query.filter_by(
            username=token_data.get('username'),
            session_token=token
        ).first()

        if not user:
            return None

        if time_now > user.session_expires:
            return None
        return user


    @login_manager.unauthorized_handler
    def unauthorized_handler():
        return json_abort(401, 'Unauthorized')

    return app


# Make this symbol available for EBS deploy
#application = create_app()
