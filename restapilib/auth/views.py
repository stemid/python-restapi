from os import urandom
from datetime import datetime, timedelta

import jwt
from flask import request, g, url_for, current_app, Blueprint
from flask.json import jsonify, loads as jsonloads
from flask import Response
from flask_login import login_user, logout_user, current_user

from restapilib import APP_NAME
from restapilib.models import db, User, Organisation
from restapilib.views import BaseView, BaseRView
from restapilib.helpers import json_abort

restapi_auth = Blueprint(APP_NAME, __name__)


class LoginView(BaseView):

    def _set_session(self, user):
        time_now = datetime.utcnow()
        session_expires = time_now + timedelta(days=2)

        user.session_token = jwt.encode(
            {
                'expires': '{isoformat}Z'.format(
                    isoformat=session_expires.isoformat()
                ),
                'username': user.username,
                'organisation_id': user.organisation
            },
            self.config.get(APP_NAME, 'secret_key'),
            algorithm='HS256'
        )
        user.session_expires = session_expires


    def post(self):
        time_now = datetime.utcnow()
        response = Response()
        req_data = jsonloads(request.data)
        username = req_data.get('username', '')
        password = req_data.get('password', '')

        if not len(username) or not len(password):
            return json_abort(500, 'Blank username or password')

        user = User.query.filter_by(username=username).first()

        if not user:
            return json_abort(404, 'User not found')

        if not user.is_enabled:
            return json_abort(401, 'User is disabled')

        try:
            user.verify_password(password)
        except Exception as e:
            return json_abort(401, 'Incorrect password, access denied')

        organisation = Organisation.query.filter_by(
            organisation_id=user.organisation
        ).first()

        if not organisation:
            return json_abort(404, 'User organisation not found, orphan!')

        if not organisation.is_enabled:
            return json_abort(401, 'User organisation is disabled')

        # This resets any existing or non-existing session with a new one
        if not user.session_expires or not user.session_token:
            self._set_session(user)
        elif time_now > user.session_expires:
            print('Session expired, re-setting')
            self._set_session(user)

        db.session.commit()
        login_user(user)

        response.headers.add(
            'authorization',
            user.session_token
        )

        return jsonify(user.as_dict())


class LogoutView(BaseView):

    def post(self):
        user = User.query.filter_by(
            user_id=current_user.user_id
        ).first()

        if not user:
            return json_abort(404, 'User not found')

        user.session_token = ''
        user.session_expires = datetime.utcnow()

        db.session.commit()
        return jsonify({
            'status': logout_user()
        })


restapi_auth.add_url_rule(
    '/login',
    view_func=LoginView.as_view('login')
)
restapi_auth.add_url_rule(
    '/logout',
    view_func=LogoutView.as_view('logout')
)
