from flask import Blueprint, jsonify, request
from flask_login import login_required

from restapilib.auth.views import BaseView, BaseRView

PLUGIN_NAME = 'sample'
app_blueprint = Blueprint(PLUGIN_NAME, __name__)

class SamplePlugin(BaseRView):
    #decorators = [login_required]

    def get(self):
        return jsonify({
            'plugin': PLUGIN_NAME,
            'echo': request.args.get('echo', '')
        })

    def post(self):
        return jsonify({
            'plugin': PLUGIN_NAME,
            'echo': request.form.get('echo', '')
        })


app_blueprint.add_url_rule(
    '/echo',
    view_func=SamplePlugin.as_view('sample'),
    methods=['POST', 'GET']
)
