from datetime import datetime
from uuid import uuid4

from sqlalchemy import inspect
from sqlalchemy import text as sa_text
from sqlalchemy.dialects.postgresql import UUID
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from nacl import pwhash

from restapilib import APP_NAME

db = SQLAlchemy()

TABLE_PREFIX = APP_NAME


class ModelAsDictMixin(object):
    """
    Get all columns of a model as a python dict for use in json.
    """

    def as_dict(self):
        """
        Convert datetime to unix epoch timestamp because they're best understood
        by the frontend vue.js code.
        """

        col_dict = {}
        for column_name, column in self._list_columns():
            # Javascript frontend works best if it gets an ISO8601 formatted
            # date with Z to indicate UTC. Or an offset of +00:00.
            # Any attempt at using a timestamp has failed, even though the
            # timestamp should be UTC already.
            if isinstance(column, datetime):
                col_data = '{iso_ts}Z'.format(
                    iso_ts=column.isoformat()
                )
            elif isinstance(column, bytes):
                # This is the password so just return an empty string for now
                col_data = ''
            else:
                col_data = column
            col_dict[column_name] = col_data
        return col_dict


    def _list_columns(self):
        for column in inspect(self).mapper.column_attrs:
            yield column.key, getattr(self, column.key)


class Organisation(db.Model, ModelAsDictMixin):
    __tablename__ = '{prefix}_organisation'.format(prefix=TABLE_PREFIX)

    organisation_id = db.Column(
        db.String(32),
        primary_key=True,
        default=lambda: uuid4().hex
    )

    name = db.Column(
        db.String(64),
        index=True
    )
    created = db.Column(db.DateTime, default=datetime.utcnow)
    is_enabled = db.Column(db.Boolean, unique=False, default=False)


class User(db.Model, UserMixin, ModelAsDictMixin):
    __tablename__ = '{prefix}_user'.format(prefix=TABLE_PREFIX)

    user_id = db.Column(
        db.String(32),
        primary_key=True,
        default=lambda: uuid4().hex
    )

    username = db.Column(
        db.String(64),
        index=True,
        unique=True
    )
    password = db.Column(db.Binary(128))
    created = db.Column(db.DateTime, default=datetime.utcnow)
    is_enabled = db.Column(db.Boolean, unique=False, default=False)
    is_administrator = db.Column(db.Boolean, unique=False, default=False)
    is_local_administrator = db.Column(
        db.Boolean,
        unique=False,
        default=False
    )
    session_token = db.Column(db.String(320), unique=True)
    session_expires = db.Column(db.DateTime, default=datetime.utcnow)

    organisation = db.Column(
        db.String(32),
        db.ForeignKey(
            '{prefix}_organisation.organisation_id'.format(
                prefix=TABLE_PREFIX
            )
        )
    )

    def get_id(self):
        return self.user_id

    def hash_password(self, password):
        self.password = pwhash.str(password.encode('utf-8'))

    def verify_password(self, password):
        return pwhash.verify(
            self.password,
            password.encode('utf-8')
        )


