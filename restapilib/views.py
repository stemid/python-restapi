from flask import current_app
from flask.views import MethodView
from flask_login import login_required


class BaseView(MethodView):

    def __init__(self, *args, **kw):
        super(BaseView, self).__init__(*args, **kw)
        self.log = current_app.logger
        self.config = current_app.config['site_config']


class BaseRView(MethodView):
    decorators = [
    	login_required,
	]

    def __init__(self, *args, **kw):
        super(BaseRView, self).__init__(*args, **kw)
        self.log = current_app.logger
        self.config = current_app.config['site_config']

