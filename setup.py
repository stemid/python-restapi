#!/usr/bin/env python3

from setuptools import setup, find_packages

setup_kwargs = {
    'name': 'python-restapi',
    'version': '1.0.0',
    'description': 'Modular Python REST-like API',
    'author': 'Stefan Midjich',
    'author_email': 'swehack@gmail.com',
    'url': 'https://gitlab.com/stemid/python-restapi',
    'license': 'GPL',
    'zip_safe': False,
    'packages': find_packages(),
    'entry_points': {
        'console_scripts': [
            'restapid = restapilib.restapid:run_api'
        ]
    },
    'scripts': [],
    'data_files': [],
    'options': {},
}

setup(**setup_kwargs)

