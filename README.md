# Python-RESTAPI

I was sick of making little microservices over and over so the intention of this is to have one central REST API service that can load modules.

This central app handles the following;

* WSGI, app and dev server for deployment.
* Basic DB models for setting up organisations and users.
* Basic views for authenticating users.
* Loading plugins from a set path.
* Some additional helper tools that can be used across plugins.

Anything else should be handled by a plugin.

# Requirements

* pipenv

Install with ``pip install --user pipenv`` on most systems, and ensure ``$HOME/.local/bin`` is in your shells ``$PATH``.

# Install

	$ pipenv --three
	$ pipenv install -r requirements.txt
	$ pipenv run python setup.py install

# Run

# Backend

Check the ``RESTAPI_CONFIG_FILE`` variable in .flaskenv first to ensure you're using the correct config file. Then run the dev server like this.

	$ pipenv run flask run

You can also pass it as an argument to restapid.

	$ pipenv run restapid -c defaults.cfg

## Deployment

First ensure the environment variable ``RESTAPI_CONFIG_FILE`` is set to the path of your configuration.

Wsgi server is under ``restapilib.wsgi.application``, most setups will only require ``restapilib.wsgi``.

# Upgrading

TODO: Example of running DB migration.

# Usage
## Create admin organisation and user

This is required to login, a first organisation is created with a user that is global administrator.

	$ pipenv run flask organisation create --help

Further config should be done via the web UI.

## Login

Use the admin password from ``create_admin_user.py``.

	$ curl -c curl.cookies -b curl.cookies -X POST \
		-d 'username=myname@gmail.com' -d 'password=secret.' 'http://localhost:5000/auth/login'

## Register

	$ curl -X POST -d 'username=myname@gmail.com' -d 'password=secret.' \
		'http://localhost:5000/auth/register'

# Examples
## Sample plugin

	$ curl -c curl.cookies -b curl.cookies \
		-X GET 'http://localhost:5000/sample/echo?echo=testing'
